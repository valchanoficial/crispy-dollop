﻿# S2B - 2018/1 - Projeto Automação de Teste de Software

Projeto Trilha Automação de Teste de Software - Students To Business 2018/1 - PUCRS.

-   Christiano Stráéhl de Vasconcelos
-   Valéria Padilha de Vargas

IDE: Eclipse

Site Testado: Moodle QA Testing Site - [https://qa.moodle.net/](https://qa.moodle.net/)

Conceitos de automação, tais como:

-   TestSuites
-   TestCases
-   Tasks
-   AppObjects
-   Framework
-   Reports
-   Datapools
-   Dataprovider
-   Categories